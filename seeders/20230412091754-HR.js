'use strict';

const { v4: uuidv4 } = require('uuid');

module.exports = {
  up: async (queryInterface, Sequelize) => {

    // Insert some initial data into the "ReffStatus" table
    await queryInterface.bulkInsert('ReffStatus', [
      { id: 1, title: 'Active' },
      { id: 2, title: 'Resigned' },
      { id: 3, title: 'Terminated' }
    ]);
    // Insert some initial data into the "ReffLevels" table
    await queryInterface.bulkInsert('ReffLevels', [
      { id: 1, title: 'Junior' },
      { id: 2, title: 'Intermediate' },
      { id: 3, title: 'Senior' }
    ]);

    // Add some sample data to the "employee" table
    const now = new Date();
    await queryInterface.bulkInsert('Employees', [
      {
        id: uuidv4(),
        name: 'John Doe',
        email: 'johndoe@example.com',
        phone: '555-1234',
        gender: 'M',
        DoB: '1990-01-01',
        joinDate: '2015-01-01',
        leaveDate: null,
        statusId: 1,
        levelId: 3,
        createdAt: now,
        updatedAt: now
      },
      {
        id: uuidv4(),
        name: 'Jane Smith',
        email: 'janesmith@example.com',
        phone: '555-5678',
        gender: 'F',
        DoB: '1995-06-15',
        joinDate: '2018-03-01',
        leaveDate: null,
        statusId: 1,
        levelId: 1,
        createdAt: now,
        updatedAt: now
      },
      {
        id: uuidv4(),
        name: 'Bob Johnson',
        email: 'bjohnson@example.com',
        phone: '555-9876',
        gender: 'M',
        DoB: '1985-12-31',
        joinDate: '2010-02-15',
        leaveDate: '2022-03-31',
        statusId: 2,
        levelId: 2,
        createdAt: now,
        updatedAt: now
      }
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Employees');
    await queryInterface.dropTable('ReffLevels');
    await queryInterface.dropTable('ReffStatus');
  }
};