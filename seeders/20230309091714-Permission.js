'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Permissions', [
      {
        permission: 'all',
        action: 'manage',
        type: 'web',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        permission: 'create',
        action: 'manage',
        type: 'api',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        permission: 'read',
        action: 'manage',
        type: 'api',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        permission: 'update',
        action: 'manage',
        type: 'api',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        permission: 'delete',
        action: 'manage',
        type: 'api',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Permissions', null, {});
  }
};
