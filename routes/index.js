var express = require("express");
var router = express.Router();
const {
  errorHandler,
  successHandler,
  encryptData,
  decryptData,
} = require("../middleware/globalMiddleware");
const EmployeeController = require("../controllers/HR/EmployeeController");
const UserController = require("../controllers/HR/UserController");
const AuthController = require("../controllers/Auth/authController");
/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Express" });
});

router.get("/decrypt", function (req, res, next) {
  try {
    const { decrypt } = req.body;
    const record = decryptData(decrypt);

    res.locals.message = "Auth success";
    res.locals.statusCode = 200;
    res.locals.data = record;
    successHandler(req, res, next);
  } catch (err) {
    return errorHandler(err, req, res, next);
  }
});

// HRD Dashboard Route
router.get("/dashboard/emp", EmployeeController.getAllEmployees);
router.get("/dashboard/user", UserController.getAllUser);
router.get("/dashboard/totalemploye", EmployeeController.getTotalEmployee);
router.get("/dashboard/countcompanies", EmployeeController.getCountCompanies);
router.get("/dashboard/hireleave", EmployeeController.employeeHireAndLeft);
router.get("/dashboard/lengthofservice", EmployeeController.getLengtOfService);
router.get("/dashboard/employeelevel", EmployeeController.employeeLevelGroup);
router.get("/dashboard/companygroup", EmployeeController.getCompanyGroup);
router.get("/dashboard/detailcompany/:id", EmployeeController.getDetailCompany);
router.get(
  "/dashboard/getemployee",
  EmployeeController.getEmployeeByNameAndCompany
);
// router.post("/dashboard/news", EmployeeController.getNews);
router.get("/dashboard/news", EmployeeController.getNews);
router.get("/dashboard/promo", EmployeeController.getPromo);
router.get("/getEmployee/:nip", EmployeeController.getEmployeeById);
router.post("/login/auth", AuthController.loginHR);
module.exports = router;
