var express = require('express');
var router = express.Router();
const { validateRequest } = require('../middleware/globalMiddleware');
const UIemployeeController = require('../controllers/UI/UIemployeeController');
const authMiddleware = require('../middleware/authMiddleware');

// Role 
/* GET company listing. */
router.get('/getCompany/', UIemployeeController.getCompany);

// AUTH 
module.exports = router;
