var express = require('express');
var router = express.Router();
const { validateRequest } = require('../middleware/globalMiddleware');
const UIJobController = require('../controllers/UI/UIJobController');
const authMiddleware = require('../middleware/authMiddleware');

// Role 
/* GET company listing. */
router.get('/listJob/', UIJobController.listJob);

// AUTH 
module.exports = router;
