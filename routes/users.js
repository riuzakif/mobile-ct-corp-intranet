var express = require('express');
const {validateRequest } = require('../middleware/globalMiddleware');
var router = express.Router();
const UserController = require('../controllers/UserController');

/* GET users listing. */
router.get('/', UserController.get);

// POST users
router.post('/', UserController.validation('post'), validateRequest, UserController.post);
// UPDATE users
router.patch('/:id', UserController.update);
// DELETE users
router.delete('/:id', function (req, res, next) {
});

module.exports = router;
