const express = require("express");
const router = express.Router();
const { faker } = require('@faker-js/faker');
const moment = require('moment');
const model = require('../models/index');
const _ = require('lodash');
const {QueryTypes} = require("sequelize");

/* GET home page. */
router.get('/', async (req, res, next) => {

  // generate ref number
  const query = `WITH RECURSIVE parent_codes AS (
    SELECT id, label, parentId, description, code::text, code::text AS full_code
    FROM treenodes
    WHERE parentId IS NULL
    UNION ALL
    SELECT t.id, t.label, t.parentId, t.description, t.code::text, p. full_code || t.code::text
    FROM treenodes t
           JOIN parent_codes p ON t.parentId = p.id
  )
     SELECT full_code as code
     FROM parent_codes 
     where description in ('Divisions')
     ORDER BY full_code;
     `;
  let refNumber = await model.sequelize.query(query, { type: QueryTypes.SELECT });

  const users = [];
  for (let i = 0; i < 13000; i++) {
    let DoB = moment(faker.date.birthdate({ min: 19, max: 68, mode: 'age' })).format('YYYY-MM-DD');
    let JoinDate = moment(faker.date.birthdate({ min: 1995, max: 2023, mode: 'year' })).format('YYYY-MM-DD');
    let statusId = Math.floor(Math.random() * 3) + 1;

    const randomIndex = Math.floor(Math.random() * refNumber.length);
    const randomElement = refNumber[randomIndex];
    const parsedRefNumber = randomElement['code'].match(/.{1,3}/g); // ['01', '01', '02', '02', '03']

    let leaveDate = null;
    let minDate   = moment(JoinDate).format('YYYY');
    if(statusId != 1){
      leaveDate = moment(faker.date.birthdate({ min: parseInt(minDate), max: 2023, mode: 'year' })).format('YYYY-MM-DD');
    }
    let nip = await generateUniqueNIP(JoinDate);

    const user = {
      nip: nip,
      name: faker.name.fullName(),
      email: faker.random.alphaNumeric(5) + faker.internet.email(),
      address: faker.address.streetAddress(),
      phone: faker.phone.number(),
      gender: faker.name.sex().substring(0, 1).toUpperCase(),
      DoB: DoB,
      joinDate: JoinDate,
      leaveDate: leaveDate,
      statusId: statusId, // 5
      levelId: Math.floor(Math.random() * 3) + 1, // 5
      employee_ref: randomElement['code'],
      employee_group: parsedRefNumber[0],
      employee_department: parsedRefNumber[1],
      employee_company: parsedRefNumber[2],
      employee_directors: parsedRefNumber[3],
      employee_division: parsedRefNumber[4],
    };
    users.push(user);
  }
  // divide the users array into chunks of 1000
  const userChunks = _.chunk(users, 1000);

  // bulk insert the data into the database
  for (const chunk of userChunks) {
    await model.Employees.bulkCreate(chunk);
  }
  // await model.Employees.bulkCreate(users);
  res.json("ok");
});


async function generateUniqueNIP(joinDates) {
  const now = moment();
  const joinDate = moment(joinDates);
  const joinYearMonth = joinDate.format('YYMM');

  let nip;

  // keep generating a new NIP until it is unique
  do {
    const randomNum = generateRandom8DigitNumber().padStart(4, '0');
    nip = joinYearMonth + randomNum;
  } while (await nipExists(nip)); // check if the generated NIP already exists

  return nip;
}

async function nipExists(nip) {
  try {
    const user = await model.Employees.findOne({
      where: {
        nip: nip,
      },
    });
    return user !== null;
  } catch (error) {
    console.error('Error checking NIP existence:', error);
    return true; // assume the NIP already exists to prevent generating duplicates
  }
}

function generateRandom8DigitNumber() {
  const min = 1e7; // 10 million
  const max = 1e8 - 1; // 99 million
  const randomNum = Math.floor(Math.random() * (max - min + 1)) + min;
  return randomNum.toString();
}

module.exports = router;
