// Default const
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { validationResult } = require("express-validator");
const {
  errorHandler,
  successHandler,
  encryptData,
} = require("../../middleware/globalMiddleware");
const { body, check } = require("express-validator");
const model = require("../../models/index");
const { checkPhoneNumber } = require("../HR/EmployeeController");
const { sendRequest } = require("../../lib/ApiHelper");

// Define the whitelist of allowed columns with aliases
const allowedColumns = ["name"];

exports.validation = (method) => {
  switch (method) {
    case "validate": {
      return [
        body("username", "username doesn`t exists").exists(),
        body("password", "password doesn`t exists").exists(),
      ];
    }
  }
};

exports.login = async (req, res, next) => {
  const { username, password } = req.body;
  try {
    // const user = await model.User.findOne({ username });
    const user = await model.User.findOne({
      attributes: ["id", "password", "username", "name", "email", "role"],
      where: {
        username: username,
      },
      include: {
        model: model.Role,
        include: {
          model: model.Permission,
          attributes: ["permission", "action", "type"],
        },
      },
    });
    if (!user) {
      return res.status(400).json({ message: "Invalid username or password" });
    }
    // console.log(user.password, user);
    // return
    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
      return res.status(400).json({ message: "Invalid username or password" });
    }

    const payload = {
      user: {
        id: user.id,
        username: user.username,
      },
    };

    if (user && isMatch) {
      const token = jwt.sign(payload, process.env.JWT_SECRET, {
        expiresIn: "1h",
      });

      const listPermission = [];
      for (const perm of user.Role.Permissions) {
        listPermission.push({
          permission: perm.permission,
          action: perm.action,
          type: perm.type,
        });
      }

      // save user token
      var response = {
        token,
        userData: encryptData({
          id: user.id,
          role: user.role,
          fullname: user.name,
          username: user.username,
          email: user.email,
        }),
        listPermission: encryptData(listPermission),
      };
      // res.locals.message = 'Auth success';
      // res.locals.statusCode = 200;
      // res.locals.data = response;
    }

    // successHandler(req, res, next);
    return res.status(200).json(response);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
};

exports.verify = async (req, res, next) => {
  try {
    const token = req.headers["x-access-token"];
    if (!token) {
      return res.status(403).json("A token is required for authentication");
      // return res.status(403).json(error("Error", res.statusCode, 'A token is required for authentication'));
    }
    try {
      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      // console.log(decoded);
      // return
      const user = await model.User.findOne({
        where: {
          id: decoded.user.id,
        },
        include: {
          model: model.Role,
          include: {
            model: model.Permission,
            attributes: ["permission", "action", "type"],
          },
        },
      });
      if (user === null) {
        throw new Error("User Not Found");
      }
      var listPermission = [];
      for (const perm of user.Role.Permissions) {
        listPermission.push({
          permission: perm.permission,
          action: perm.action,
          type: perm.type,
        });
      }
      var response = {
        userData: encryptData({
          id: user.id,
          role: user.role,
          fullname: user.name,
          username: user.username,
          email: user.email,
        }),
        listPermission: encryptData(listPermission),
      };
      // res.locals.message = 'Verify success';
      // res.locals.statusCode = 200;
      // res.locals.data = response;
      return res.status(200).json(response);
      // return successHandler(req, res, next);
    } catch (err) {
      // return res.status(401).json(error("Error", res.statusCode, err.message));
      return errorHandler(err, req, res, next);
    }
  } catch (err) {
    return errorHandler(err, req, res, next);
    // return res.status(401).json(error("Error", res.statusCode, 'x-access-token not found'));
  }
};

exports.loginHR = async (req, res, next) => {
  try {
    let authResult;
    let register;
    switch (req.body.type) {
      case "MPC":
        register = await checkPhoneNumber(req.body.phone_no);
        if (register) {
          authResult = await this.mpcLogin(
            req.body.phone_no,
            req.body.password
          );
        } else {
          return res.status(400).json({
            error: true,
            message: "number not register or wrong password",
          });
        }
        break;
      case "LDAB":
        break;
      case "OTP":
        break;
      default:
        break;
    }

    // res.locals.data = authResult;

    // Call successHandler middleware function
    // successHandler(req, res, next);
    return res.status(200).json(authResult);
  } catch (err) {
    // Call errorHandler middleware function with error object
    console.log(err);
    errorHandler(err, req, res, next);
  }
};

exports.mpcLogin = async (phoneNo, password) => {
  try {
    // const url = process.env.AUTH_LOGIN_MPC_URL;
    const url = "https://mocki.io/v1/f450ecea-779e-445c-8ca5-e34fb5b66a7b";
    const apidata = await sendRequest(
      "GET",
      url,
      {
        "Content-type": "application/json",
      },
      false,
      false
    );
    // const apidata = await sendRequest(
    //   "POST",
    //   url,
    //   {
    //     "Content-type": "application/json",
    //   },
    //   { phone_no: phoneNo, password: password },
    //   false
    // );
    return apidata;
  } catch (error) {}
};
