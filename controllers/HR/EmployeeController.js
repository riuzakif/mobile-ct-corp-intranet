const { sequelize } = require("../../models");
const { QueryTypes, Model, Op, fn, INTEGER } = require("sequelize");
const model_raw = require("../../models");
const model = require("../../models").Employees;
const modelNews = require("../../models").News;
const modelPromo = require("../../models").Promo;
const { success, error, validation } = require("../../lib/ResponseApi");
const modelDB = require("../../models/index");
const { includes, isUndefined } = require("lodash");
const {
  successHandler,
  errorHandler,
} = require("../../middleware/globalMiddleware");

exports.getAllEmployees = async (req, res) => {
  try {
    const item = await sequelize.query("select * from getallemployee()", {
      type: QueryTypes.SELECT,
    });
    return res.status(200).json(success("success", item, 200));
  } catch (msg) {
    throw Error(msg.message);
  }
};

exports.getTotalEmployee = async (req, res) => {
  try {
    const item = await sequelize.query("select * from fx()", {
      type: QueryTypes.SELECT,
    });
    item.map((e) => {
      e.malePercent = (
        (parseInt(e.male) / parseInt(e.total_employee)) *
        100
      ).toFixed(2);
      e.malePercent = Math.round(e.malePercent);
      e.femalePercent = (
        (parseInt(e.female) / parseInt(e.total_employee)) *
        100
      ).toFixed(2);
      e.femalePercent = Math.round(e.femalePercent);
    });
    console.log(item);

    return res.status(200).json(success("success", item, 200));
  } catch (msg) {
    throw Error(msg.message);
  }
};

exports.getCountCompanies = async (req, res) => {
  try {
    const item = await sequelize.query("select * from countcompanies()", {
      type: QueryTypes.SELECT,
    });
    return res.status(200).json(success("success", item, 200));
  } catch (msg) {
    throw Error(msg.message);
  }
};

exports.employeeHireAndLeft = async (req, res) => {
  const year = req.query.year;
  if (!year) {
    return res
      .status(400)
      .json(error("Add the correct year in url", 400, true, []));
  }
  try {
    const item = await sequelize.query(
      `SELECT * FROM generate_monthly_totals(${year})`,
      {
        type: QueryTypes.SELECT,
      }
    );
    return res.status(200).json(success("success", item, 200));
  } catch (msg) {
    throw Error(msg.message);
  }
};

exports.getLengtOfService = async (req, res) => {
  try {
    const item = await sequelize.query("select * from lengthofservice()", {
      type: QueryTypes.SELECT,
    });
    return res.status(200).json(success("success", item, 200));
  } catch (msg) {
    throw Error(msg.message);
  }
};

exports.employeeLevelGroup = async (req, res) => {
  try {
    // modelDB.belongsTo(ReffLevels, { foreignKey: 'levelId' });
    const foundItem = await model.findAll({
      attributes: [
        [sequelize.fn("COUNT", sequelize.col("ReffLevel.title")), "count"],
        [sequelize.col("ReffLevel.title"), "title"],
      ],
      // limit: 10,
      include: [
        {
          model: modelDB.ReffLevels,
          attributes: [],
        },
      ],
      where: {
        leaveDate: null,
      },
      group: ["ReffLevel.title"],
      raw: true,
    });
    return res.status(200).json(success("success", foundItem, 200));
  } catch (msg) {
    throw Error(msg.message);
  }
};

exports.getCompanyGroup = async (req, res) => {
  try {
    const item = await sequelize.query("select * from groupcompanies()", {
      type: QueryTypes.SELECT,
    });
    var currParentid = item[0].departement_id;
    var obj = { departement_label: item[0].departement_label };
    obj.company_list = [];
    const groupcompanies = [obj];
    console.log(groupcompanies);
    var currIndex = 0;
    item.map((e) => {
      if (e.departement_id === currParentid) {
        // let name = e.departement_label.toLowerCase().replace(/ /g, "_");
        if (groupcompanies[currIndex].company_list) {
          groupcompanies[currIndex].company_list.push({
            company_id: e.company_id,
            company_label: e.company_label,
            image_name: e.image_name,
            employee_count: e.employee_count,
          });
        }
      } else {
        // let name = e.departement_label.toLowerCase().replace(/ /g, "_");
        currParentid = e.departement_id;
        currIndex += 1;
        obj = {};
        obj.departement_label = e.departement_label;
        obj.company_list = [];
        groupcompanies.push(obj);
        if (groupcompanies[currIndex].company_list) {
          groupcompanies[currIndex].company_list.push({
            company_id: e.company_id,
            company_label: e.company_label,
            image_name: e.image_name,
            employee_count: e.employee_count,
          });
        }
      }
    });
    return res.status(200).json(success("success", groupcompanies, 200));
  } catch (msg) {
    throw Error(msg.message);
  }
};

exports.getDetailCompany = async (req, res) => {
  try {
    const id = req.params.id;
    const item = await sequelize.query(`select * from detailcompany(${id})`, {
      type: QueryTypes.SELECT,
    });
    // console.log( item)
    const buildHierarchy = (parentId) => {
      const children = item
        .filter((item) => item.parentid === parentId)
        .map((item) => {
          const child = buildHierarchy(item.id);
          return {
            ...item,
            sum:
              parseInt(item.employee_division_count) + sumChildrenSums(child),
            child,
          };
        });

      return children.length ? children : [];
    };

    const sumChildrenSums = (children) => {
      return children.reduce((sum, child) => sum + child.sum, 0);
    };

    // Call the buildHierarchy function with the root parent id
    const result = buildHierarchy(parseInt(id));
    // const record = sumEmployeeDivisionCount(result);
    return res.status(200).json(success("success", result, 200));
  } catch (msg) {
    throw Error(msg.message);
  }
};

exports.getEmployeeByNameAndCompany = async (req, res) => {
  // console.log(req.query);
  // return;
  try {
    const query = req.query;
    // console.log(req.body);
    if (
      req.query.employee_name == undefined ||
      req.query.company_code == undefined
    ) {
      return res
        .status(400)
        .json(error("Parameter Can Not Be Null", 400, true, []));
    }
    // console.log(req.body);
    const item = await sequelize.query(
      `select * from search_employees('${query.employee_name}','${query.company_code}')`,
      {
        type: QueryTypes.SELECT,
      }
    );

    return res.status(200).json(success("success", item, 200));
  } catch (msg) {
    throw Error(msg.message);
  }
};

exports.getNews = async (req, res) => {
  try {
    // modelDB.belongsTo(ReffLevels, { foreignKey: 'levelId' });
    if (
      req.query.page == undefined ||
      (req.query.page.toLowerCase() != "home" &&
        req.query.page.toLowerCase() != "detail")
    ) {
      return res
        .status(400)
        .json(
          error(
            "Page must not be null, page = home OR page =detail",
            400,
            true,
            []
          )
        );
    }
    const imgUrl = process.env.IMG_URL;
    let limit = 0;
    let attribute = [
      "id",
      "title",
      "image",
      [sequelize.literal(`'${imgUrl}' || "image"`), "image"],
      [
        sequelize.fn("to_char", sequelize.col("published_at"), "DD Mon YYYY"),
        "published_at",
      ],
      "content",
    ];
    console.log(attribute);
    if (req.query.page.toLowerCase() == "home") {
      limit = 3;
      // attribute.push("url");
    } else if (req.query.page.toLowerCase() == "detail") {
      limit = 10;
      // attribute.push("url");
    }
    const foundItem = await modelNews.findAll({
      attributes: attribute,
      order: [["createdAt", "DESC"]],
      limit: limit,
    });

    return res.status(200).json(success("success", foundItem, 200));
  } catch (msg) {
    console.log(msg);
    throw Error(msg.message);
  }
};

exports.getPromo = async (req, res) => {
  try {
    // modelDB.belongsTo(ReffLevels, { foreignKey: 'levelId' });
    const foundItem = await modelPromo.findAll({
      attributes: [
        "id",
        "url",
        "title",
        "slug",
        "description",
        "image",
        [
          sequelize.fn("to_char", sequelize.col("start_date"), "DD Mon YYYY"),
          "start_date",
        ],
      ],
      order: [["start_date", "DESC"]],
    });
    return res.status(200).json(success("success", foundItem, 200));
  } catch (msg) {
    throw Error(msg.message);
  }
};

exports.getEmployeeById = async (req, res, next) => {
  try {
    const nip = req.params.nip;
    const record = await sequelize.query(
      `SELECT * FROM masked_employee_details_by_nip('${nip}')`,
      {
        type: QueryTypes.SELECT,
      }
    );
    if (record.length !== 0) {
      // Set response data and message in res.locals
      res.locals.data = record;
      res.locals.message = "Success";
    } else {
      // Set response data and message in res.locals
      res.locals.message = "Data is empty";
      res.locals.data = {};
    }

    // Call successHandler middleware function
    successHandler(req, res, next);
  } catch (err) {
    // Call errorHandler middleware function with error object
    console.log(err);
    errorHandler(err, req, res, next);
  }
};

exports.checkPhoneNumber = async (phoneNo) => {
  try {
    const foundItem = await model.findOne({
      where: {
        phone: phoneNo,
      },
    });
    // Call successHandler middleware function
    if (foundItem != null && foundItem != undefined) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    // Call errorHandler middleware function with error object
    console.log(err);
    errorHandler(err, req, res, next);
  }
};
