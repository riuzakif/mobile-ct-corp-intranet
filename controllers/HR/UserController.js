const db = require("../../models");
const model = require("../../models").User;
const { success, error, validation } = require("../../lib/ResponseApi");

exports.getAllUser = async (req, res) => {
  try {
    const foundItem = await model.findAll();
    return res.status(200).json(success("success", foundItem, 200));
  } catch (msg) {
    throw Error(msg.message);
  }
};
