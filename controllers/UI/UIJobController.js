// Default const 
const { errorHandler, successHandler, validateRequest } = require('../../middleware/globalMiddleware');
const model = require('../../models/index');
const { dynamicUpdateData, dynamicStoreData, dynamicCreateOrUpdate } = require('../../middleware/CRUDMiddleware');
const {sequelize} = require("../../models");
const {QueryTypes} = require("sequelize"); // <-- Require the middleware

// Display list of all Authors.

exports.listJob = async (req,res,next) => {
    try {
        const record = await model.MasterJobs.findAll({
            attributes: { exclude: ['createdAt', 'updatedAt', 'created_by', 'updated_by'] },
            include: {
                attributes: ['label', 'icon', 'code'],
                model: model.treenodes,
                as: 'company',
            },
            order: [['createdAt', 'DESC']],
            limit: 10
        });
        if (record.length !== 0) {
            // Set response data and message in res.locals
            res.locals.data = record;
            res.locals.message = 'Success';
        } else {
            // Set response data and message in res.locals
            res.locals.message = 'Data is empty';
            res.locals.data = {};
        }

        // Call successHandler middleware function
        successHandler(req, res, next);
    } catch (err) {
        // Call errorHandler middleware function with error object
        console.log(err);
        errorHandler(err, req, res, next);
    }
}