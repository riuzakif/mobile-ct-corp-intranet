// Default const 
const { errorHandler, successHandler, validateRequest } = require('../../middleware/globalMiddleware');
const model = require('../../models/index');
const { dynamicUpdateData, dynamicStoreData, dynamicCreateOrUpdate } = require('../../middleware/CRUDMiddleware');
const {sequelize} = require("../../models");
const {QueryTypes} = require("sequelize"); // <-- Require the middleware

// Display list of all Authors.
exports.getCompany = async (req, res,next) => {
    try {
        const record = await sequelize.query(`select 'All' as code, 'Semua Perusahaan' as label UNION ALL select code, label from treenodes where "level" = 3 GROUP BY code, label `, {
            type: QueryTypes.SELECT,
        });
        if (record.length !== 0) {
            // Set response data and message in res.locals
            res.locals.data = record;
            res.locals.message = 'Success';
        } else {
            // Set response data and message in res.locals
            res.locals.message = 'Data is empty';
            res.locals.data = {};
        }

        // Call successHandler middleware function
        successHandler(req, res, next);
    } catch (err) {
        // Call errorHandler middleware function with error object
        console.log(err);
        errorHandler(err, req, res, next);
    }
};