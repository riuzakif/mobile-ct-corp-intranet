'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('MasterLevels', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      parentCompany: {
        allowNull: false,
        type: Sequelize.STRING
      },
      // groupCode: {
      //   allowNull: false,
      //   type: Sequelize.STRING
      // },
      // companyCode: {
      //   allowNull: false,
      //   type: Sequelize.STRING
      // },
      // directorCode: {
      //   allowNull: false,
      //   type: Sequelize.STRING
      // },
      // departmentCode: {
      //   allowNull: false,
      //   type: Sequelize.STRING
      // },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('MasterLevels');
  }
};
