'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('MasterLevels', 'groupCode', {
      type: Sequelize.STRING,
      references: {
        model: 'ReffLevelGroups',
        key: 'code'
      }
    });
    await queryInterface.addColumn('MasterLevels', 'companyCode', {
      type: Sequelize.STRING,
      references: {
        model: 'ReffLevelCompanies',
        key: 'code'
      }
    });
    await queryInterface.addColumn('MasterLevels', 'directorCode', {
      type: Sequelize.STRING,
      references: {
        model: 'ReffLevelDirectors',
        key: 'code'
      }
    });
    await queryInterface.addColumn('MasterLevels', 'divisionCode', {
      type: Sequelize.STRING,
      references: {
        model: 'ReffLevelDivisions',
        key: 'code'
      }
    });
    await queryInterface.addColumn('MasterLevels', 'departmentCode', {
      type: Sequelize.STRING,
      references: {
        model: 'ReffLevelDepartments',
        key: 'code'
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('MasterLevels', 'groupCode');
    await queryInterface.removeColumn('MasterLevels', 'companyCode');
    await queryInterface.removeColumn('MasterLevels', 'directorCode');
    await queryInterface.removeColumn('MasterLevels', 'divisionCode');
    await queryInterface.removeColumn('MasterLevels', 'departmentCode');
  }
};
