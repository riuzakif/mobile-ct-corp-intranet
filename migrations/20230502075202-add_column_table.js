'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('Employees', 'employee_ref', {
      type: Sequelize.STRING,
      allowNull: true
    });
    await queryInterface.addColumn('Employees', 'employee_group', {
      type: Sequelize.STRING,
      allowNull: true
    });
    await queryInterface.addColumn('Employees', 'employee_department', {
      type: Sequelize.STRING,
      allowNull: true
    });
    await queryInterface.addColumn('Employees', 'employee_company', {
      type: Sequelize.STRING,
      allowNull: true
    });
    await queryInterface.addColumn('Employees', 'employee_directors', {
      type: Sequelize.STRING,
      allowNull: true
    });
    await queryInterface.addColumn('Employees', 'employee_division', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('Employees', 'employee_ref');
    await queryInterface.removeColumn('Employees', 'employee_group');
    await queryInterface.removeColumn('Employees', 'employee_department');
    await queryInterface.removeColumn('Employees', 'employee_company');
    await queryInterface.removeColumn('Employees', 'employee_directors');
    await queryInterface.removeColumn('Employees', 'employee_division');
  }
};
