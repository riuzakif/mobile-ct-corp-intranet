'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  // Rreate_reffLevelCompanies_table.js
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('ReffLevelCompanies', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      code: {
        type: Sequelize.STRING,
        allowNull: false
      },
      title: {
        type: Sequelize.STRING,
        allowNull: false
      },
      description: Sequelize.STRING,
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE
    });
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.dropTable('ReffLevelCompanies');
  }
};
