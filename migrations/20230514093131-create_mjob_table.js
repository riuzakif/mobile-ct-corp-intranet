'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('MasterJobs', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false
      },
      company_code: {
        type: Sequelize.STRING
      },
      title: {
        allowNull: false,
        type: Sequelize.STRING
      },
      location: {
        allowNull: false,
        type: Sequelize.STRING
      },
      type: {
        allowNull: false,
        type: Sequelize.STRING
      },
      email: {
        allowNull: false,
        type: Sequelize.STRING
      },
      job_about: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      job_description: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      job_requirement: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      other_description: {
        type: Sequelize.TEXT
      },
      status_email: {
        allowNull: true,
        type: Sequelize.STRING
      },
      post_date: {
        allowNull: false,
        type: Sequelize.DATEONLY
      },
      expiry_date: {
        allowNull: false,
        type: Sequelize.DATEONLY
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      created_by: {
        type: Sequelize.UUID
      },
      updated_by: {
        type: Sequelize.UUID
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('MasterJobs');
  }
};
