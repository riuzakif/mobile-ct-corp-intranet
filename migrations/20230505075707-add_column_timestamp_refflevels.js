"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn("ReffLevels", "createdAt", {
      type: Sequelize.DATE,
      allowNull: true,
    });
    await queryInterface.addColumn("ReffLevels", "updatedAt", {
      type: Sequelize.DATE,
      allowNull: true,
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeColumn("ReffLevels", "updatedAt");
    await queryInterface.removeColumn("ReffLevels", "createdAt");
  },
};
