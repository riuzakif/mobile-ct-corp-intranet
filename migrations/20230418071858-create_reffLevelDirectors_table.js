'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  // create_Rreate_reDirectors_table.js
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('ReffLevelDirectors', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      code: {
        type: Sequelize.STRING,
        allowNull: false
      },
      title: {
        type: Sequelize.STRING,
        allowNull: false
      },
      description: Sequelize.STRING,
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE
    });
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.dropTable('ReffLevelDirectors');
  }
};
