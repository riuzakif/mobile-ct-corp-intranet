'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addConstraint('ReffLevelGroups', {
      type: 'unique',
      fields: ['code']
    });
    await queryInterface.addConstraint('ReffLevelCompanies', {
      type: 'unique',
      fields: ['code']
    });
    await queryInterface.addConstraint('ReffLevelDirectors', {
      type: 'unique',
      fields: ['code']
    });
    await queryInterface.addConstraint('ReffLevelDivisions', {
      type: 'unique',
      fields: ['code']
    });
    await queryInterface.addConstraint('ReffLevelDepartments', {
      type: 'unique',
      fields: ['code']
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeConstraint('ReffLevelGroups', 'ReffLevelGroups_code_key');
    await queryInterface.removeConstraint('ReffLevelCompanies', 'ReffLevelCompanies_code_key');
    await queryInterface.removeConstraint('ReffLevelDirectors', 'ReffLevelDirectors_code_key');
    await queryInterface.removeConstraint('ReffLevelDivisions', 'ReffLevelDivisions_code_key');
    await queryInterface.removeConstraint('ReffLevelDepartments', 'ReffLevelDepartments_code_key');
  }
};
