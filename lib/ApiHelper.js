const axios = require("axios");

exports.sendRequest = (method, url, headers, body, proxy) => {
  return new Promise((resolve, reject) => {
    var config = {
      method: method,
      url: url,
      headers: headers,
    };
    if (proxy) {
      config.proxy = proxy;
    }
    if (body) {
      var data = JSON.stringify(body);
      config.data = data;
    }
    axios(config)
      .then(function (response) {
        resolve({
          error: false,
          data: response.data,
        });
      })
      .catch(function (error) {
        // console.log(error)
        reject({
          error: true,
          data: error,
        });
      });
  });
};
