exports.updateOrCreate = async (model, where, newItem) => {
  // First try to find the record
  try {
    const foundItem = await model.findOne(where);
    if (!foundItem) {
      // Item not found, create a new one
      const item = await model.create(newItem);
      return {
        item: item,
        created: true,
      };
    }
    // Found an item, update it
    const item = await model.update(newItem, where);
    return {
      item: foundItem,
      created: false,
    };
  } catch (msg) {
    throw Error(msg.message);
  }
};

exports.firstOrCreate = async (model, where, newItem) => {
  // First try to find the record
  try {
    const foundItem = await model.findOne(where);
    if (!foundItem) {
      // Item not found, create a new one
      const item = await model.create(newItem);
      return {
        item: item,
        created: true,
      };
    }
    // Found an item, update it
    // const item = await model.update(newItem, where);
    return {
      item: foundItem,
      created: false,
    };
  } catch (msg) {
    throw Error(msg.message);
  }
};
