const jwt = require("jsonwebtoken");
const model = require("../models/index");
const { errorHandler, successHandler, validateRequest } = require('../middleware/globalMiddleware');

const verifyToken = async (req, res, next) => {
    try {
        let token = req.body.token || req.query.token || req.headers["x-access-token"];
        token = token.split(' ')[1];

        if (!token) {
            // return res.status(403).send("A token is required for authentication");
            // return res.status(403).json(error("Error", res.statusCode, 'A token is required for authentication'));
            res.locals.message = 'A token is required for authentication';
            res.locals.statusCode = 403;
            return successHandler(req, res, next);
        }

        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const user = await model.User.findOne({
            attributes: ['id', 'username', 'name', 'email', 'role'],
            where: {
                id: decoded.user.id,
            },
            include: {
                model: model.Role,
                include: {
                    model: model.Permission,
                    attributes: ['permission', 'action', 'type']
                }
            }
        });
        // return
        if (user === null) {
            throw new Error('User Not Found');
        }
        // console.log(user.Role.Permission);
        var listPermission = [];
        for (const perm of user.Role.Permissions) {
            await listPermission.push({
                permission: perm.permission,
                action: perm.action,
                type: perm.type,
            })
        }
        // console.log(listPermission);
        var userData = {
            id: user.id,
            role: user.role,
            fullname: user.name,
            username: user.username,
            email: user.email,
        }
        // delete user.Role;
        req.user = {
            userData: userData,
            listPermission: listPermission,
        }
    } catch (err) {
        return errorHandler(err, req, res, next);
    }
    return next();
};

module.exports = verifyToken;