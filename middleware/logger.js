const fs = require('fs');
const morgan = require('morgan');
const path = require('path');
const expressWinston = require('express-winston');
const winston = require('winston');
const moment = require('moment');

// Define log directory
const logDirectory = path.join(__dirname, '..', 'logs');

// Define a middleware function to log outgoing responses
const logOutgoingResponses = (req, res, next) => {
    // console.log(req.method);
    // return
    const logStream = fs.createWriteStream(path.join(__dirname, '../logs/requestResponse_' + moment().format('YYYYMM') + '.log'), { flags: 'a' });

    const originalSend = res.send;
    res.send = function (body) {
        console.log(body); // log the response body
        const formattedDate = moment().format('YYYY-MM-DD HH:mm:ss');
        originalSend.call(this, body);
        logStream.write(`{"level": "info", "method": "` + req.method + `", "url": "` + req.url + `","response":` + body + `,"statusCode": ` + res.statusCode + `, "responseTime": ` + res.responseTime + `,"timestamp":"` + formattedDate + `"}\n`);
    };
    
    next();
};

// Create middleware function to log requests and responses
const logRequestResponse = expressWinston.logger({
    transports: [
        new winston.transports.File({
            filename: path.join(logDirectory, 'requestResponse_' + moment().format('YYYYMM') + '.log'),
            format: winston.format.combine(
                winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
                winston.format.json()
            ),
        })
    ],
    json: false,
    meta: true,
    msg: 'HTTP {{req.method}} {{req.url}} {{res.statusCode}} {{res.responseTime}}ms',
    expressFormat: true,
    colorize: false,
    ignoreRoute: function (req, res) { return false; }
});

const errorHandlerMiddleware = (error, req, res, next) => {
    console.error(error);

    const { method, originalUrl } = req;
    const { ip } = req.headers;
    const { user } = req;
    const status = error.status || 500;
    const message = error.message || 'Internal Server Error';

    const logLine = `[${new Date()}] ${method} ${originalUrl} from ${ip} by ${user ? user.username : 'anonymous'} - ERROR ${status} - ${message}\n`;

    fs.appendFile(path.join(logDirectory, 'error-access.log'), logLine, (error) => {
        if (error) {
            console.error(`Error writing to log file: ${error.message}`);
        }
    });

    res.status(status).send({
        message: message,
    });
};


module.exports = {
    logOutgoingResponses,
    logRequestResponse,
    errorHandlerMiddleware
};