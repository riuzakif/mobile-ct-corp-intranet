'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class masterLevels extends Model {
        static associate(models) {
            masterLevels.belongsTo(models.ReffLevelGroups, { foreignKey: 'groupCode' });
            masterLevels.belongsTo(models.ReffLevelCompanies, { foreignKey: 'companyCode' });
            masterLevels.belongsTo(models.ReffLevelDirectors, { foreignKey: 'directorCode' });
            masterLevels.belongsTo(models.ReffLevelDivisions, { foreignKey: 'divisionCode' });
            masterLevels.belongsTo(models.ReffLevelDepartments, { foreignKey: 'departmentCode' });
        }
    };
    masterLevels.init({
        parentCompany: DataTypes.STRING,
        groupCode: DataTypes.STRING,
        companyCode: DataTypes.STRING,
        directorCode: DataTypes.STRING,
        departmentCode: DataTypes.STRING
    }, {
        sequelize,
        modelName: 'masterLevels',
    });
    return masterLevels;
};
