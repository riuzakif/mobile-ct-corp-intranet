'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class ReffLevelCompanies extends Model {
        static associate(models) {
            // Define associations here, if any
        }
    };
    ReffLevelCompanies.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        code: {
            type: DataTypes.STRING,
            allowNull: false
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false
        },
        description: DataTypes.STRING,
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'ReffLevelCompanies',
        tableName: 'ReffLevelCompanies'
    });
    return ReffLevelCompanies;
};
