'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class ReffLevels extends Model {
        static associate(models) {
            // Define associations here, if any
        }
    };
    ReffLevels.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'ReffLevels',
        tableName: 'ReffLevels'
    });
    return ReffLevels;
};
