"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Employees extends Model {
    static associate(models) {
      Employees.belongsTo(models.ReffLevelGroups, {
        foreignKey: "employee_group", // assuming 'employee_group' is the foreign key column in the 'Employees' table that references the primary key of the 'ReffLevelGroups' table
        targetKey: "code", // specify 'code' as the target key since it's the primary key of the 'ReffLevelGroups' table
      });
      Employees.belongsTo(models.ReffLevelDepartments, {
        foreignKey: "employee_department", // assuming 'employee_group' is the foreign key column in the 'Employees' table that references the primary key of the 'ReffLevelGroups' table
        targetKey: "code", // specify 'code' as the target key since it's the primary key of the 'ReffLevelGroups' table
      });
      Employees.belongsTo(models.ReffLevelCompanies, {
        foreignKey: "employee_company", // assuming 'employee_group' is the foreign key column in the 'Employees' table that references the primary key of the 'ReffLevelGroups' table
        targetKey: "code", // specify 'code' as the target key since it's the primary key of the 'ReffLevelGroups' table
      });
      Employees.belongsTo(models.ReffLevelDirectors, {
        foreignKey: "employee_directors", // assuming 'employee_group' is the foreign key column in the 'Employees' table that references the primary key of the 'ReffLevelGroups' table
        targetKey: "code", // specify 'code' as the target key since it's the primary key of the 'ReffLevelGroups' table
      });
      Employees.belongsTo(models.ReffLevels, {
        foreignKey: "levelId", // assuming 'employee_group' is the foreign key column in the 'Employees' table that references the primary key of the 'ReffLevelGroups' table
        targetKey: "id", // specify 'code' as the target key since it's the primary key of the 'ReffLevelGroups' table
      });
      Employees.belongsTo(models.ReffLevelDivisions, {
        foreignKey: "employee_division", // assuming 'employee_group' is the foreign key column in the 'Employees' table that references the primary key of the 'ReffLevelGroups' table
        targetKey: "code", // specify 'code' as the target key since it's the primary key of the 'ReffLevelGroups' table
      });
    }
  }
  Employees.init(
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
      },
      name: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING(100),
        allowNull: false,
        unique: true,
      },
      phone: {
        type: DataTypes.STRING(20),
        allowNull: false,
      },
      gender: {
        type: DataTypes.CHAR(2),
        allowNull: false,
      },
      DoB: {
        type: DataTypes.DATEONLY,
        allowNull: false,
      },
      joinDate: {
        type: DataTypes.DATEONLY,
        allowNull: false,
      },
      leaveDate: {
        type: DataTypes.DATEONLY,
        allowNull: true,
      },
      statusId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "ReffStatus",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "RESTRICT",
      },
      levelId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "ReffLevels",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "RESTRICT",
      },
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE,
      employee_ref: DataTypes.STRING,
      employee_group: DataTypes.STRING,
      employee_department: DataTypes.STRING,
      employee_company: DataTypes.STRING,
      employee_directors: DataTypes.STRING,
      employee_division: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Employees",
      tableName: "Employees",
    }
  );
  return Employees;
};
