'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Role extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Role.belongsTo(models.User, {
        foreignKey: 'name',
      })

      Role.belongsToMany(models.Permission, {
        through: models.RolePermission,
        foreignKey: 'RoleId',
      })

    }
  }
  Role.init({
    name: DataTypes.STRING,
    is_active: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Role',
  });

  return Role;
};

