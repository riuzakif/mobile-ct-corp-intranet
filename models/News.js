'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class News extends Model {}

    News.init({
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4,
            allowNull: false
        },
        url: {
            type: DataTypes.STRING,
            allowNull: false
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false
        },
        slug: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        content: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        image: {
            type: DataTypes.STRING
        },
        published_at: {
            type: DataTypes.DATE
        }
    }, {
        sequelize,
        modelName: 'News',
        tableName: 'News',
        timestamps: true,
        createdAt: 'createdAt',
        updatedAt: 'updatedAt'
    });

    return News;
};
