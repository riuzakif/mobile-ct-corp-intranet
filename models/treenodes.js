'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class TreeNode extends Model {
        static associate(models) {
            // Define associations here
        }
    }

    TreeNode.init(
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            label: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            parentid: {
                type: DataTypes.INTEGER,
                allowNull: true,
            },
            description: {
                type: DataTypes.TEXT,
                allowNull: true,
            },
            code: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            level: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            icon: {
                type: DataTypes.STRING,
                allowNull: true,
            },
        },
        {
            sequelize,
            modelName: 'treenodes',
            tableName: "treenodes",
            timestamps: false // Disable timestamps
        }
    );

    return TreeNode;
};
