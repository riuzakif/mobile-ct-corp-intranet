'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class MasterJobs extends Model {
        static associate(models) {
            // Define associations here
            this.belongsTo(models.treenodes, {
                foreignKey: 'company_code', // column in MasterJobs table
                targetKey: 'code', // column in TreeNode table
                as: 'company', // alias for the association
            });
        }
    }

    MasterJobs.init(
        {
            id: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                allowNull: false,
            },
            company_code: {
                type: DataTypes.STRING,
            },
            title: {
                allowNull: false,
                type: DataTypes.STRING,
            },
            location: {
                allowNull: false,
                type: DataTypes.STRING,
            },
            type: {
                allowNull: false,
                type: DataTypes.STRING,
            },
            email: {
                allowNull: false,
                type: DataTypes.STRING,
            },
            job_about: {
                allowNull: false,
                type: DataTypes.TEXT,
            },
            job_description: {
                allowNull: false,
                type: DataTypes.TEXT,
            },
            job_requirement: {
                allowNull: false,
                type: DataTypes.TEXT,
            },
            other_description: {
                type: DataTypes.TEXT,
            },
            status_email: {
                allowNull: true,
                type: DataTypes.STRING,
            },
            post_date: {
                allowNull: false,
                type: DataTypes.DATEONLY,
            },
            expiry_date: {
                allowNull: false,
                type: DataTypes.DATEONLY,
            },
            createdAt: {
                allowNull: false,
                type: DataTypes.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: DataTypes.DATE,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
        },
        {
            sequelize,
            modelName: 'MasterJobs',
        }
    );

    return MasterJobs;
};
