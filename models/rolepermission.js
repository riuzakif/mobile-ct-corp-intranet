'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RolePermission extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      RolePermission.belongsTo(models.Role, {
        targetKey: 'id',
        foreignKey: 'RoleId',
        constraints: false,
        as: 'Role'
      });
      RolePermission.belongsTo(models.Permission, {
        targetKey: 'id',
        foreignKey: 'PermissionId',
        constraints: false,
        as: 'Permission'
      });
    }
  }
  RolePermission.init({
    RoleId: DataTypes.INTEGER,
    PermissionId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'RolePermission',
  });
  return RolePermission;
};